# NLGenomeSweeper



search a genome for NBS-LRR (NLR) disease resistance genes based on the presence of the NB-ARC domain using the consensus sequence of the Pfam HMM profile (PF00931) and class specific consensus sequences built from Vitis vinifera. https://doi.org/10.